<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('voyages');
});

/* Route::get('/about', function() {
    return view('about')->with('phrase', 'une phrase pour tester');
}); */

Route::get('/messages', function() {
    return 'messages';
});

/* Route::get('/voyages/{id}', 'VoyageController@show'); */


Route::group(['prefix' => 'admin'], function() {
    // Route::get('voyages/{id}', 'VoyageController@showVoyage');
    // Route::get('voyages', 'VoyageController@create');
    // Route::post('voyages/storage', 'VoyageController@store');
    Route::resource('voyages', 'VoyageController');
    Route::get('voyages/{id}/delete', 'VoyageController@destroy');
    Route::post('voyages', 'VoyageController@search');
/*     Route::get('voyages', 'VoyageController@index'); */
});

Route::get('voyage/{id}', 'VoyageController@showVoyage');

/* Route::group(['prefix' => 'user'], function() {
    Route::get('voyages', function() {});
}); */

/* Route::get('/voyages', function () {
    return view('voyages');
}); */

Auth::routes();

/* Route::get('/home', 'HomeController@index')->name('home'); */

