<?php

use Faker\Generator as Faker;

$factory->define(App\Photo::class, function (Faker $faker) {
    return [
        'url' => $faker->imageUrl(),
        'voyage_id' => $faker->numberBetween(1, 120)
    ];
});
