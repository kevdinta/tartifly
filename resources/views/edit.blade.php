<form method="post" action="{{route('voyages.update', ['voyage' => $voyage->id])}}">
    @csrf
    @method('PUT')
    @if ($errors->has('libelle'))
     {{ $errors->first('libelle')}}
    @endif
    @if ($errors->has('pays'))
     {{ $errors->first('pays')}}
    @endif
    @if ($errors->has('ville'))
    {{ $errors->first('ville')}}
    @endif
    @if ($errors->has('date_debut'))
    {{ $errors->first('date_debut')}}
    @endif
    @if ($errors->has('date_fin'))
    {{ $errors->first('date_fin')}}
    @endif
    @if ($errors->has('cout'))
    {{ $errors->first('cout')}}
    @endif
    @if ($errors->has('photo'))
    {{ $errors->first('photo')}}
   @endif
    @if ($errors->has('description'))
    {{ $errors->first('description')}}
    @endif
<input type="text" name="libelle" placeholder="libelle" value="{{ $voyage->libelle }}">
<input type="text" name="pays" placeholder="pays" value="{{ $voyage->pays }}">
    <input type="text" name="ville" placeholder="ville" value="{{ $voyage->ville }}">
    <input type="datetime" name="date_debut" value="{{ $voyage->date_debut }}">
<input  type="datetime" name="date_fin" value="{{ $voyage->date_fin }}">
<input type="number" name="cout" placeholder="cout" value="{{ $voyage->cout }}">
    <input type="text" name="photo" placeholder="photo" value="{{ $voyage->photo }}">
    <textarea name="description" placeholder="description">{{ $voyage->description }}</textarea>
    <select name="disponibilite"> 
        <option value="0" @if($voyage->disponibilite == 0) selected @endif>0</option>
        <option value="1" @if($voyage->disponibilite == 1) selected @endif>1</option>
    </select>
   <input type="submit" value="save">
   </form>