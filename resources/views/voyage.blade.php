@extends('layouts.tartifly')
@section('content')
<!-- destination est la fonction appelée dans le modèle Destination -->
 @isset($specific_voyage->destination->destination)  
  {{ $specific_voyage->destination->destination}}
 @endisset
 <!-- photos est la fonction appelée dans le modèle Photo -->
 @isset($specific_voyage->photos)
   @foreach($specific_voyage->photos as $img)
   <img src="{{ $img->url }}"/>
   @endforeach
 @endisset
@endsection